#!/usr/bin/env python

import requests
import sys
import json
from os.path import expanduser
home = expanduser("~")

apiKey = '4RsjaXIOET8xanuS9WBBj6JMayPrWxgQ8UdP9sCl'

if sys.argv[1] == 'update':
	f = open(home + "/.organiser/todo.yaml")
	contents = f.read()

	response = requests.put("https://fww6wvru0g.execute-api.ap-southeast-2.amazonaws.com/dev/todo", data = contents, headers = {'x-api-key' : apiKey})

	print(response)
	if response.status_code == 204:
		print("All good")
	else:
		print(response.text)

if sys.argv[1] == 'todo':
	response = requests.get("https://fww6wvru0g.execute-api.ap-southeast-2.amazonaws.com/dev/priorities", headers = {'x-api-key' : apiKey})
	tasks = json.loads(response.text)

	for task in tasks:
		print(task['name'])

if sys.argv[1] == 'urgency':
	response = requests.get("https://fww6wvru0g.execute-api.ap-southeast-2.amazonaws.com/dev/urgency", headers = {'x-api-key' : apiKey})
	print(response.text)

