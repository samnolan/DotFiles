#!/usr/bin/bash

Battery(){
  echo $(acpi --battery | cut -d, -f2)
}
Clock(){
  echo $(date "+%a %b %d, %H:%M")
}

while true; do
  echo "%{r} $(Battery) $(Clock)"
  sleep 1
done
