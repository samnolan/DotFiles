# http://code.stapelberg.de/git/i3status/tree/contrib/wrapper.pl
#
# To use it, ensure your ~/.i3status.conf contains this line:
#     output_format = "i3bar"
# in the 'general' section.
# Then, in your ~/.i3/config, use:
#     status_command i3status | ~/i3status/contrib/wrapper.py
# In the 'bar' section.
#
# In its current version it will display the cpu frequency governor, but you
# are free to change it to display whatever you like, see the comment in the
# source code below.
#
# © 2012 Valentin Haenel <valentin.haenel@gmx.de>
#
# This program is free software. It comes without any warranty, to the extent
# permitted by applicable law. You can redistribute it and/or modify it under
# the terms of the Do What The Fuck You Want To Public License (WTFPL), Version
# 2, as published by Sam Hocevar. See http://sam.zoy.org/wtfpl/COPYING for more
# details.

import sys
import json
import requests
import datetime
import subprocess
import json
import urllib3

apiKey = "4RsjaXIOET8xanuS9WBBj6JMayPrWxgQ8UdP9sCl"
organiserApi = "https://3mhxujix8f.execute-api.ap-southeast-2.amazonaws.com/dev/"

scriptFile = "~/.scripts/run_organiser"
bookScriptFile = "~/.scripts/run_books"
def get_governor():
    """ Get the current governor for cpu0, assuming all CPUs use the same. """
    with open('/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor') as fp:
        return fp.readlines()[0].strip()

def get_urgency():
    try:
        urgency = get_organiser("urgency")
        bait = get_organiser("bait")
        velocity = get_organiser("velocity")
        return str(velocity) + "<" + str(urgency) + ">" + str(bait)
    except:
        return '-'

def get_organiser(command):
    try:
        response = requests.get(organiserApi + command).text
        if len(response) > 0:
            return json.loads(response)
        else:
            return 0
    except:
        return subprocess.getoutput(scriptFile + " " + command).strip()

def get_todo():
    try:
        first_task = get_organiser('first')
        if isinstance(first_task, str):
            return first_task
        else:
            return first_task['name']
    except:
        return "-"

def get_books():
    response = subprocess.getoutput(bookScriptFile + " started") 
    if len(response) > 0:
        books = response.split("\n")

        pageCounts = subprocess.getoutput(bookScriptFile + " page").split("\n")

        started_books = []
        for i in range(len(books)):
            started_books.append(books[i] + ", " + pageCounts[i])

        return started_books
    else:
        return []
    
def get_meditations_left():
    reference_date = datetime.datetime(2018, 4, 10)
    since_reference = datetime.datetime.now() - reference_date
    days_since_reference = since_reference.total_seconds() / (60 * 60 * 24)

    minutes_until_finished_start = 4 * 60 + 52
    minutes_per_day = minutes_until_finished_start / 13
    minutes_should_be_up_to = minutes_until_finished_start - minutes_per_day * days_since_reference
    hours = int(minutes_should_be_up_to / 60)
    minutes = int(minutes_should_be_up_to % 60)
    return str(hours) + ":" + str(minutes)


def print_line(message):
    """ Non-jsonFile = requests.get("https://fww6wvru0g.execute-api.ap-southeast-2.amazonaws.com/dev/priorities", headers={"x-api-key": apiKey}).json()
    return jsonFile[0].nambuffered printing to stdout. """
    sys.stdout.write(message + '\n')
    sys.stdout.flush()

def read_line():
    """ Interrupted respecting reader for stdin. """
    # try reading a line, removing any extra whitespace
    try:
        line = sys.stdin.readline().strip()
        # i3status sends EOF, or an empty line
        if not line:
            sys.exit(3)
        return line
    # exit on ctrl-c
    except KeyboardInterrupt:
        sys.exit()

if __name__ == '__main__':
    # Skip the first line which contains the version header.
    print_line(read_line())

    # The second line contains the start of the infinite array.
    print_line(read_line())

    wasBreakTime = False
    while True:
        line, prefix = read_line(), ''
        # ignore comma at start of lines
        if line.startswith(','):
            line, prefix = line[1:], ','

        j = json.loads(line)
        # insert information into the start of the json, but could be anywhere
        # CHANGE THIS LINE TO INSERT SOMETHING ELSE
        now = datetime.datetime.now()
        currentMinute = (now.minute)
        isBreakTime = currentMinute % 30 >= 25


        if isBreakTime:
            j.insert(0, {'full_text' : '%s' % get_urgency(), 'name' : 'urg', 'color': '#859900'})
            j.insert(0, {'full_text' : '%s' % get_todo(), 'name' : 'tod', 'color': '#859900'})
            if wasBreakTime == False:
                subprocess.call(["xbacklight", '-set', '0'])
        else:
            j.insert(0, {'full_text' : '%s' % get_urgency(), 'name' : 'urg', 'color': '#268bd2'})
            j.insert(0, {'full_text' : '%s' % get_todo(), 'name' : 'tod', 'color': '#268bd2'})
            if wasBreakTime == True:
                subprocess.run(["xbacklight", '-set', '1'])

        started_books = get_books()
        for book in started_books:
            j.insert(0, {'full_text': '%s' % book, 'name': book[:2], 'color': '#268bd2'})
        wasBreakTime = isBreakTime
        # and echo back new encoded json
        print_line(prefix + json.dumps(j));

